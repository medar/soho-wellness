<footer class="site-footer">
	<div class="container">
		<div class="footer-top">
			<div class="footer-top-item">
				<h3>Pages</h3>
				<?php
				wp_nav_menu( array(
						'theme_location' => 'menu-2',
						'menu_id'        => 'footer-menu',
						'container'      => '',
						'menu_class'     => ''
					)

				);
				?>
			</div>
			<div class="footer-top-item">
				<h3>Instagram: @sohowellness</h3>
				<div class="social-widtget-wrapper">
					<?php echo do_shortcode( '[instagram-feed]' ); ?>
				</div>
			</div>
			<div class="footer-top-item">
				<?php
				$facebook = '';
				if ( function_exists( 'ot_get_option' ) ) :
					$facebook = ot_get_option( 'facebook_page' );
				endif;
				?>
				<h3>Soho Wellness’ Facebook</h3>
				<div class="social-widtget-wrapper">
					<div id="fb-root"></div>
					<script>(function (d, s, id) {
                            var js, fjs = d.getElementsByTagName(s)[0];
                            if (d.getElementById(id)) return;
                            js = d.createElement(s);
                            js.id = id;
                            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.10";
                            fjs.parentNode.insertBefore(js, fjs);
                        }(document, 'script', 'facebook-jssdk'));
					</script>
					<div class="fb-page" data-href="<?php echo $facebook ?>" data-tabs=""
						 data-small-header="false" data-adapt-container-width="true" data-hide-cover="false"
						 data-show-facepile="true">
						<blockquote cite="<?php echo $facebook ?>" class="fb-xfbml-parse-ignore"></blockquote>
					</div>
				</div>
			</div>
		</div>
		<?php
		$footer_logo = '';
		if ( function_exists( 'ot_get_option' ) ) :
			$footer_logo = ot_get_option( 'footer_logo' );
		endif;
		?>
		<div class="footer-bottom">
			<a href="/"><img src="<?php echo $footer_logo; ?>"
							 srcset="<?php echo $footer_logo; ?> 1x, <?php echo $footer_logo ?> 2x"
							 width="108" height="42" alt=""></a>
			<p><?
				if ( function_exists( 'ot_get_option' ) ) {
					echo $address_line_1 = ot_get_option( 'site_copyright' );
				} ?>
			</p>
		</div>
	</div>
</footer>
<nav class="all-sites" id="allSites">
	<?php
	if ( function_exists( 'ot_get_option' ) ) {
		$links['mdg']['title']      = ot_get_option( 'mdg_title', '#' );
		$links['mdg']['link']       = ot_get_option( 'mdg_link', '#' );
		$links['dd']['title']       = ot_get_option( 'dd_title', '#' );
		$links['dd']['link']        = ot_get_option( 'dd_link', '#' );
		$links['bpct']['title']     = ot_get_option( 'business_performance_coaching_title', '#' );
		$links['bpct']['link']      = ot_get_option( 'business_performance_coaching_links', '#' );
		$links['swds']['title']     = ot_get_option( 'soho_wellness___day_spa_title', '#' );
		$links['swds']['link']      = ot_get_option( 'soho_wellness___day_spa_link', '#' );
		$links['wandc']['title']    = ot_get_option( 'workshops_and_classes_title', '#' );
		$links['wandc']['link']     = ot_get_option( 'workshops_and_classes_link', '#' );
		$links['pmatcher']['title'] = ot_get_option( 'professional_matcher_title', '#' );
		$links['pmatcher']['link']  = ot_get_option( 'professional_matcher_link', '#' );
	}
	?>
	<ul>
		<li><a href="<?php echo $links['mdg']['link']; ?>">
				<picture>
					<source srcset="<?php echo get_template_directory_uri(); ?>/img/tabs/MDG_small.png 1x, <?php echo get_template_directory_uri(); ?>/img/tabs/MDG_small@2x.png 2x"
							media="(min-width: 768px)">
					<source srcset="<?php echo get_template_directory_uri(); ?>/img/tabs/MDG_xsmall.png 1x, <?php echo get_template_directory_uri(); ?>/img/tabs/MDG_xsmall@2x.png 2x"
							media="(max-width: 767px)">
					<img src="<?php echo get_template_directory_uri(); ?>/img/tabs/MDG_small.png"
						 alt="<?php echo $links['mdg']['title']; ?>" title="<?php echo $links['mdg']['title']; ?>">
				</picture>
			</a></li>
		<li><a href="<?php echo $links['dd']['link']; ?>">
				<picture>
					<source srcset="<?php echo get_template_directory_uri(); ?>/img/tabs/DD_small.png 1x, <?php echo get_template_directory_uri(); ?>/img/tabs/DD_small@2x.png 2x"
							media="(min-width: 768px)">
					<source srcset="<?php echo get_template_directory_uri(); ?>/img/tabs/DD_xsmall.png 1x, <?php echo get_template_directory_uri(); ?>/img/tabs/DD_xsmall@2x.png 2x"
							media="(max-width: 767px)">
					<img src="<?php echo get_template_directory_uri(); ?>/img/tabs/DD_small.png"
						 alt="<?php echo $links['dd']['title']; ?>" title="<?php echo $links['dd']['title']; ?>">
				</picture>
			</a></li>
		<li><a href="<?php echo $links['bpct']['link']; ?>">
				<picture>
					<source srcset="<?php echo get_template_directory_uri() . '/'; ?>/img/tabs/BC_small.png 1x, <?php echo get_template_directory_uri(); ?>/img/tabs/BC_small@2x.png 2x"
							media="(min-width: 768px)">
					<source srcset="<?php echo get_template_directory_uri() . '/'; ?>/img/tabs/BC_xsmall.png 1x, <?php echo get_template_directory_uri(); ?>/img/tabs/BC_xsmall@2x.png 2x"
							media="(max-width: 767px)">
					<img src="<?php echo get_template_directory_uri(); ?>/img/tabs/BC_small.png"
						 alt="<?php echo $links['bpct']['title']; ?>" title="<?php echo $links['bpct']['title']; ?>">
				</picture>
			</a></li>
		<li><a href="<?php echo $links['swds']['link']; ?>">
				<picture>
					<source srcset="<?php echo get_template_directory_uri(); ?>/img/tabs/SW_small.png 1x, <?php echo get_template_directory_uri(); ?>/img/tabs/SW_small@2x.png 2x"
							media="(min-width: 768px)">
					<source srcset="<?php echo get_template_directory_uri(); ?>/img/tabs/SW_xsmall.png 1x, <?php echo get_template_directory_uri(); ?>/img/tabs/SW_xsmall@2x.png 2x"
							media="(max-width: 767px)">
					<img src="<?php echo get_template_directory_uri(); ?>/img/tabs/SW_small.png"
						 alt="<?php echo $links['swds']['title']; ?>" title="<?php echo $links['swds']['title']; ?>">
				</picture>
			</a></li>
		<li><a href="<?php echo $links['wandc']['link']; ?>">
				<picture>
					<source srcset="<?php echo get_template_directory_uri(); ?>/img/tabs/WS_small.png 1x, <?php echo get_template_directory_uri(); ?>/img/tabs/WS_small@2x.png 2x"
							media="(min-width: 768px)">
					<source srcset="<?php echo get_template_directory_uri(); ?>/img/tabs/WS_xsmall.png 1x, <?php echo get_template_directory_uri(); ?>/img/tabs/WS_xsmall@2x.png 2x"
							media="(max-width: 767px)">
					<img src="<?php echo get_template_directory_uri(); ?>/img/tabs/WS_small.png"
						 alt="<?php echo $links['wandc']['title']; ?>" title="<?php echo $links['wandc']['title']; ?>">
				</picture>
			</a></li>
		<li><a href="<?php echo $links['pmatcher']['link']; ?>">
				<picture>
					<source srcset="<?php echo get_template_directory_uri(); ?>/img/tabs/PM_small.png 1x, <?php echo get_template_directory_uri(); ?>/img/tabs/PM_small@2x.png 2x"
							media="(min-width: 768px)">
					<source srcset="<?php echo get_template_directory_uri(); ?>/img/tabs/PM_xsmall.png 1x, <?php echo get_template_directory_uri(); ?>/img/tabs/PM_xsmall@2x.png 2x"
							media="(max-width: 767px)">
					<img src="<?php echo get_template_directory_uri(); ?>/img/tabs/PM_small.png"
						 alt="<?php echo $links['pmatcher']['title']; ?>"
						 title="<?php echo $links['pmatcher']['title']; ?>">
				</picture>
			</a></li>
	</ul>
</nav>
<?php wp_footer(); ?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-105191689-1', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>
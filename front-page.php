<?php get_header(); ?>
<main class="site-main site-main--index">
	<div class="index-slider">
		<div class="index-slider__items">
			<?php
			$args   = array( 'posts_per_page' => - 1, 'offset' => 0, 'post_type' => 'sw_slider' );
			$slides = get_posts( $args );
			$iSlide = 0;
			?>
			<?php foreach ( $slides as $slide ) : $iSlide++;?>

				<div class="index-slide">
					<div class="index-slide__img">
						<picture>
							<?php echo wp_get_attachment_image( get_field( 'image_slider', $slide->ID ),
								$size = array( 1440, 540 )
							); ?>
						</picture>
						<div class="index-slide__text">
							<h2><?php echo esc_html( get_the_title( $slide->ID ) ); ?></h2><a
									href="<?php if ( function_exists( 'ot_get_option' ) ) {
echo ot_get_option( 'schedule_your_appointment', '#' );
} ?>" target="_blank">Schedule
								your appointment</a>
						</div>
					</div>
				</div>
			<?php endforeach; ?>

		</div>
		<?php if($iSlide > 1){ ?>
		<div class="index-slider__controls">
			<div class="container"><span class="index-slider__left">
              <svg xmlns="http://www.w3.org/2000/svg" width="7" height="13" data-name="PageArrow" viewbox="0 0 7 13">
                <path class="pageFill"
					  d="M.91 6.5l6-5.87a.36.36 0 0 0 0-.52.38.38 0 0 0-.53 0L.11 6.24a.36.36 0 0 0 0 .52l6.25 6.13a.38.38 0 0 0 .26.11.37.37 0 0 0 .26-.11.36.36 0 0 0 0-.52z"></path>
              </svg></span><span class="index-slider__right">
              <svg xmlns="http://www.w3.org/2000/svg" width="7" height="13" data-name="PageArrow" viewbox="0 0 7 13">
                <path class="pageFill"
					  d="M.91 6.5l6-5.87a.36.36 0 0 0 0-.52.38.38 0 0 0-.53 0L.11 6.24a.36.36 0 0 0 0 .52l6.25 6.13a.38.38 0 0 0 .26.11.37.37 0 0 0 .26-.11.36.36 0 0 0 0-.52z"></path>
              </svg></span></div>
		</div>
		<?php } ?>
	</div>
	<?php sw_tabs(); ?>
	<div class="index-main-text">


		<div class="container">
			<div class="index-main-text__wrapper">
				<div class="index-main-text__text">
					<?php echo wpautop($post->post_content); ?>
					<a href="<?php echo get_post_meta( $post->ID, 'button_link', true ); ?>"><?php echo get_post_meta( $post->ID, 'button_text', true); ?></a>
				</div>
				<div class="index-main-text__img">
					<picture>
						<?php echo wp_get_attachment_image(get_post_meta( $post->ID, 'front_image', true), $size=array(560,470)); ?>
					</picture>
				</div>
			</div>
		</div>
	</div>
	<div class="service-list">
		<div class="container">
			<h2><?php echo get_post_meta( $post->ID, 'services_header', true); ?></h2>
			<p><?php echo get_post_meta( $post->ID, 'services_text', true); ?></p>
			<div class="service-list__wrapper">
				<?php echo sw_services_posts(); ?>
			</div>
		</div>
	</div>
	<div class="latest-blog-posts">
		<div class="container">
			<div class="latest-blog-posts__wrapper">
				<?php echo sw_recent_posts( $posts_count = 3, $posts_offset = 0 ); ?>
			</div>
		</div>
	</div>
	<?php sw_subscribe(); ?>
</main>
<?php get_footer(); ?>

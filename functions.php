<?php
/**
 * lm functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 */

if ( ! function_exists( 'sw_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function sw_setup() {
		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );
		/*
		 * This theme uses wp_nav_menu() in one location.
		 */
		register_nav_menus( array(
			'menu-1' => esc_html( 'Primary' ),
			'menu-2' => esc_html( 'Footer menu' )
		) );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );
		add_image_size( 'slider', 1440, 540, true );
		add_image_size( 'post-header-full', 1170, 500 );
		add_image_size( 'post-header-small', 480, 480 );
		add_image_size( 'post-preview', 340, 300, true );
		add_image_size( 'front-page', 560, 470 );
		add_image_size( 'services-preview', 160, 160 );
		add_image_size( 'specials', 400, 340, true );
		add_image_size( 'tours', 570, 370, true );
	}
endif;
add_action( 'after_setup_theme', 'sw_setup' );

if ( ! function_exists( 'sw_scripts' ) ) :
	function sw_scripts() {

		wp_enqueue_style(
			'sw-libs',
			get_template_directory_uri() . '/css/libs.css',
			array(),
			'1.0.0',
			'all'
		);

		wp_enqueue_style(
			'sw-styles',
			get_template_directory_uri() . '/css/styles.css',
			array( 'sw-libs' ),
			'1.0.0',
			'all'
		);

		wp_enqueue_style(
			'matchmaker-fonts',
			'https://fonts.googleapis.com/css?family=Open+Sans:400,400i,600,700|Roboto+Condensed:700',
			array(),
			'1.0.0',
			'all'
		);

		wp_enqueue_style(
			'sw-style',
			get_stylesheet_uri(),
			array( 'sw-libs', 'sw-styles' ),
			'1.0.0',
			'all'
		);

		wp_enqueue_script(
			'sw-bootstrap',
			'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js',
			array( 'jquery' ),
			'3.3.6',
			true
		);

		wp_enqueue_script(
			'sw-libs',
			get_template_directory_uri() . '/js/libs.js',
			array( 'sw-bootstrap' ),
			'1.0.0',
			true
		);

		wp_enqueue_script(
			'sw-js',
			get_template_directory_uri() . '/js/scripts.js',
			array( 'sw-libs' ),
			'1.0.0',
			true
		);

	}
endif;

add_action( 'wp_enqueue_scripts', 'sw_scripts' );

function sw_recent_single_posts_tmp( $post, $post_image, $excerpt_length, $time ) {
	$content = $post->post_content;
	$excerpt = wp_trim_words( $content, $num_words = $excerpt_length, $more = null );
	$res     = '';

	if ( $post_image ) :
		$res .= '<div class="first-blog-post__img"><a class="latest-blog-item__img" href="' . get_permalink( $post->ID ) . '">'
		        . get_the_post_thumbnail( $post->ID, $size = 'post-preview' )
		        . '</div></a>';
	endif;

	$res .= '<div class="first-blog-post__text">
					<a href="' . get_permalink( $post->ID ) . '"><h3>' . $post->post_title . '</h3></a>'
	        . '<p>' . $excerpt . '</p>'
	        . '<span>' . $time . '</span>';

	return $res;
}

function sw_recent_posts_tmp( $post, $post_image, $excerpt_length, $blog_card_no_img, $time ) {
	$content = $post->post_content;
	$excerpt = wp_trim_words( $content, $num_words = $excerpt_length, $more = null );

	$res = '<div class="latest-blog-item' . $blog_card_no_img . '">';

	if ( $post_image ) :
		$res .= '<a class="latest-blog-item__img" href="' . get_permalink( $post->ID ) . '">'
		        . get_the_post_thumbnail( $post->ID, $size = 'post-preview' )
		        . '</a>';
	endif;

	$res .= '<a href="' . get_permalink( $post->ID ) . '"><h3>' . $post->post_title . '</h3></a>'
	        . '<p>' . $excerpt . '</p>'
	        . '<span>' . $time . '</span>';
	$res .= '</div>';

	return $res;
}

function sw_recent_posts( $posts_count = 3, $posts_offset = 0, $category_name = 'blog', $order_by = 'date' ) {
	$res   = '';
	$order = '';
	if ( $order_by == 'date' ) {
		$order = "DESC";
	} elseif ( $order_by == 'title' ) {
		$order = "ASC";
	}
	$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;

	$args  = array(
		'posts_per_page'   => $posts_count,
		'post_type'        => 'post',
		'category_name'    => $category_name,
		'post_status'      => 'publish',
		'paged'            => $paged,
		'orderby'          => $order_by,
		'order'            => $order,
		'offset'           => $posts_offset,
		"suppress_filters" => true

	);
	$query = new WP_Query( $args );

	if ( $query->have_posts() ) {
		while ( $query->have_posts() ) {
			$query->the_post();
			$query->post->post_date;
			$time           = date( 'd M Y', strtotime( $query->post->post_date ) );
			$post_image     = false;
			$excerpt_length = 90;

			$blog_card_no_img = " blog-card__no-img";
			if ( has_post_thumbnail( $query->post->ID ) ) :
				$post_image       = true;
				$excerpt_length   = 15;
				$blog_card_no_img = '';
			endif;
			// get tmp
			if ( $posts_count > 1 ) {
				$res .= sw_recent_posts_tmp( $query->post, $post_image, $excerpt_length, $blog_card_no_img, $time );
			} else {
				$res .= sw_recent_single_posts_tmp( $query->post, $post_image, 120, $time );
				break;
			}
		}
		wp_reset_query();
		wp_reset_postdata();

		return $res;
	} else {
		wp_reset_query();
		wp_reset_postdata();

		return false;
	}
}

/**
 * Возвращает верстку одной превьюхи для стафа
 *
 * @param $post
 *
 * @return string
 */
function sw_staff_post( $post ) {
	$id             = $post->ID;
	$content        = $post->post_content;
	$excerpt_length = 10;
	$excerpt        = wp_trim_words( $content, $num_words = $excerpt_length, $more = null );
	$stafftags      = get_the_tags( $id );
	$echo_tags      = '';
	if ( $stafftags ) {
		$echo_tags .= '<h4>';
		foreach ( $stafftags as $tag ) {
			$echo_tags .= $tag->name . ', ';
		}
		$echo_tags = substr( $echo_tags, 0, - 2 );
		$echo_tags .= '</h4>';
	}
	$output = '';
	ob_start();
	?>
	<a class="about-item" href="<?php echo get_permalink( $id ) ?>">
		<?php echo get_the_post_thumbnail( $id ); ?>
		<h3><?php echo esc_html( get_the_title( $id ) ); ?></h3>
		<?php echo $echo_tags; ?>
		<p><?php echo $excerpt; ?></p>
	</a>

	<?php
	$output .= ob_get_clean();

	return $output;
}

/**
 * Возвращает верстку превьюх для стафа
 *
 * @param int    $posts_count
 * @param int    $posts_offset
 * @param string $category_name
 *
 * @return bool|string
 */
function sw_staff_posts( $posts_count = - 1, $posts_offset = 0, $category_name = 'people,staff' ) {
	$res = '';
//	$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
	$args  = array(
		'posts_per_page' => $posts_count,
//		'post_type'      => 'post',
		'category_name'  => $category_name,
		'post_status'    => 'publish',
//		'paged'          => $paged,
		'offset'         => $posts_offset
	);
	$query = new WP_Query( $args );

	if ( $query->have_posts() ) {
		while ( $query->have_posts() ) {
			$query->the_post();
			// get tmp
			$res .= sw_staff_post( $query->post );
		}
		wp_reset_query();
		wp_reset_postdata();

		return $res;
	} else {
		wp_reset_query();
		wp_reset_postdata();

		return false;
	}
}

/**
 * Возвращает верстку одной превьюхи для Services
 *
 * @param $post
 *
 * @return string
 */
function sw_services_post( $post ) {
	$id     = $post->ID;
	$output = '';
	ob_start();
	?>
	<div class="service-item">
		<a class="service-item__container" href="<?php echo get_permalink( $id ) ?>">
			<div class="service-item__front">
				<?php echo get_the_post_thumbnail( $id, array( 160, 160 ) ); ?>
			</div>
			<div class="service-item__back">
				<div class="service-item__inner">
					<p><?php echo esc_html( $post->post_title ); ?></p>
				</div>
			</div>
		</a>
	</div>
	<?php
	$output .= ob_get_clean();

	return $output;
}

/**
 * Возвращает верстку превьюх для Services
 *
 * @param int    $posts_count
 * @param int    $posts_offset
 * @param string $category_name
 *
 * @return bool|string
 */
function sw_services_posts( $posts_count = - 1, $posts_offset = 0, $category_name = 'services' ) {
	$res = '';
//	$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
	$args  = array(
		'posts_per_page' => $posts_count,
//		'post_type'      => 'post',
		'category_name'  => $category_name,
		'post_status'    => 'publish',
//		'paged'          => $paged,
		'offset'         => $posts_offset
	);
	$query = new WP_Query( $args );

	if ( $query->have_posts() ) {
		while ( $query->have_posts() ) {
			$query->the_post();
			// get tmp
			$res .= sw_services_post( $query->post );
		}
		wp_reset_query();
		wp_reset_postdata();

		return $res;
	} else {
		wp_reset_query();
		wp_reset_postdata();

		return false;
	}
}

/**
 * ajax коллбэк для infinite scroll
 */
function ajax_sw_infinite_scroll_callback() {
	$offset  = intval( $_POST['offset'] );
	$sort_by = strval( $_POST['sort_by'] );
	echo sw_recent_posts( $posts_count = 3, $posts_offset = $offset, $category_name = 'blog', $order_by = $sort_by );

	wp_die();
}

add_action( 'wp_ajax_infinite_scroll', 'ajax_sw_infinite_scroll_callback' );
add_action( 'wp_ajax_nopriv_infinite_scroll', 'ajax_sw_infinite_scroll_callback' );

function sw_search_result() {
	$output = '';
	ob_start();
	?>
	<div class="blog-content <?php if ( is_single() ): echo 'blog-content--relevant'; endif; ?>">
		<div class="container">
			<div class="blog-content__wrapper">
				<?php
				global $post;
				while ( have_posts() ) : the_post();
					$post_image       = false;
					$excerpt_lenght   = 50;
					$blog_card_no_img = " blog-card__no-img";
					if ( has_post_thumbnail( $post->ID ) ) :
						$post_image       = true;
						$excerpt_lenght   = 15;
						$blog_card_no_img = '';
					endif;

					$content = $post->post_content;
					$excerpt = wp_trim_words( $content, $num_words = $excerpt_lenght, $more = null );
					$time    = date( 'd M Y', strtotime( $post->post_date ) );
					echo '<div class="blog-card">';
					if ( $post_image ) :
						echo '<a class="blog-card__img" href="'
						     . get_permalink( $post->ID ) . '">';
						echo get_the_post_thumbnail( $post->ID, $size = 'post-preview' );

						echo '</a>';
					endif;

					echo '<div class="blog-card__text' . $blog_card_no_img . '">'
					     . '<a href="' . get_permalink( $post->ID ) . '"><h3>'
					     . $post->post_title
					     . '</h3></a>'
					     . '<p>' . $excerpt . '</p>'
					     . '</div>'

					     . '<div class="blog-card__date' . $blog_card_no_img . '">'
					     . '<p>' . $time . '</p>'
					     . '</div>'

					     . '</div>';

				endwhile;
				?>
			</div>
		</div>
	</div>
	<?php
	wp_reset_postdata();
	$output .= ob_get_clean();

	echo $output;
}

function sw_posted_on( $post_date ) {
	$time_string = date( 'd M Y', strtotime( $post_date ) );

	echo '<span class="article-date">' . $time_string . '</span>';

}

function sw_register_types() {
	register_post_type( 'sw_slider', array(
		'label'               => null,
		'labels'              => array(
			'name'               => 'Sliders',
			'singular_name'      => 'Slider',
			'add_new'            => 'Add slider',
			'add_new_item'       => 'Add slider item',
			'edit_item'          => 'Edit slider item',
			'new_item'           => 'New slider item',
			'view_item'          => 'View slider item',
			'search_items'       => 'Search slider item',
			'not_found'          => 'Not found',
			'not_found_in_trash' => 'Not found in trash',
			'parent_item_colon'  => '',
			'menu_name'          => 'Slider',
		),
		'description'         => '',
		'public'              => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => false,
		'show_in_rest'        => false,
		'rest_base'           => false,
		'menu_position'       => null,
		'menu_icon'           => 'dashicons-welcome-widgets-menus',
		//'capability_type'   => 'post',
		//'capabilities'      => 'post',
		//'map_meta_cap'      => null,
		'hierarchical'        => false,
		'supports'            => array( 'title', 'editor' ),
		// 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
		'taxonomies'          => array(),
		'has_archive'         => false,
		'rewrite'             => true,
		'query_var'           => true,
	) );
	register_post_type( 'sw_rooms', array(
		'label'               => null,
		'labels'              => array(
			'name'               => 'Rooms',
			'singular_name'      => 'Rooms',
			'add_new'            => 'Add room',
			'add_new_item'       => 'Add room item',
			'edit_item'          => 'Edit room item',
			'new_item'           => 'New room item',
			'view_item'          => 'View room item',
			'search_items'       => 'Search room item',
			'not_found'          => 'Not found',
			'not_found_in_trash' => 'Not found in trash',
			'parent_item_colon'  => '',
			'menu_name'          => 'Rooms',
		),
		'description'         => '',
		'public'              => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => false,
		'show_in_rest'        => false,
		'rest_base'           => false,
		'menu_position'       => null,
		'menu_icon'           => 'dashicons-tickets-alt',
		//'capability_type'   => 'post',
		//'capabilities'      => 'post',
		//'map_meta_cap'      => null,
		'hierarchical'        => false,
		'supports'            => array( 'title', 'editor', 'thumbnail' ),
		// 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
		'taxonomies'          => array(),
		'has_archive'         => false,
		'rewrite'             => true,
		'query_var'           => true,
	) );
	register_post_type( 'sw_packages', array(
		'label'               => null,
		'labels'              => array(
			'name'               => 'Packages',
			'singular_name'      => 'Packages',
			'add_new'            => 'Add package',
			'add_new_item'       => 'Add package item',
			'edit_item'          => 'Edit package item',
			'new_item'           => 'New package item',
			'view_item'          => 'View rpackage item',
			'search_items'       => 'Search package item',
			'not_found'          => 'Not found',
			'not_found_in_trash' => 'Not found in trash',
			'parent_item_colon'  => '',
			'menu_name'          => 'Packages',
		),
		'description'         => '',
		'public'              => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => false,
		'show_in_rest'        => false,
		'rest_base'           => false,
		'menu_position'       => null,
		'menu_icon'           => 'dashicons-products',
		//'capability_type'   => 'post',
		//'capabilities'      => 'post',
		//'map_meta_cap'      => null,
		'hierarchical'        => false,
		'supports'            => array( 'title', 'thumbnail' ),
		// 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
		'taxonomies'          => array(),
		'has_archive'         => false,
		'rewrite'             => true,
		'query_var'           => true,
	) );


}

add_action( 'init', 'sw_register_types' );

function sw_remove_autop( $content ) {
	global $post;

	if ( $post->post_type == 'sw_testimonial' ):
		remove_filter( 'the_content', 'wpautop' );
	endif;

	if ( $post->post_type == 'sw_faq' ):
		remove_filter( 'the_content', 'wpautop' );
	endif;

	if ( $post->post_type == 'sw_inspections' ):
		remove_filter( 'the_content', 'wpautop' );
	endif;

	return $content;
}

add_filter( 'the_content', 'sw_remove_autop', 0 );

function sw_tabs( $class = '' ) {
	$output = '';
	if ( function_exists( 'ot_get_option' ) ) {
		$links['mdg']['title']      = ot_get_option( 'mdg_title', '#' );
		$links['mdg']['link']       = ot_get_option( 'mdg_link', '#' );
		$links['dd']['title']       = ot_get_option( 'dd_title', '#' );
		$links['dd']['link']        = ot_get_option( 'dd_link', '#' );
		$links['bpct']['title']     = ot_get_option( 'business_performance_coaching_title', '#' );
		$links['bpct']['link']      = ot_get_option( 'business_performance_coaching_links', '#' );
		$links['swds']['title']     = ot_get_option( 'soho_wellness___day_spa_title', '#' );
		$links['swds']['link']      = ot_get_option( 'soho_wellness___day_spa_link', '#' );
		$links['wandc']['title']    = ot_get_option( 'workshops_and_classes_title', '#' );
		$links['wandc']['link']     = ot_get_option( 'workshops_and_classes_link', '#' );
		$links['pmatcher']['title'] = ot_get_option( 'professional_matcher_title', '#' );
		$links['pmatcher']['link']  = ot_get_option( 'professional_matcher_link', '#' );
	}
	?>
	<div class="index-tabs <?php echo $class; ?>">
		<ul>
			<li><a href="<?php echo $links['mdg']['link']; ?>">
					<picture>
						<source srcset="<?php echo get_template_directory_uri(); ?>/img/tabs/MDG.png 1x, <?php echo get_template_directory_uri(); ?>/img/tabs/MDG@2x.png 2x"
								media="(min-width: 1200px)">
						<source srcset="<?php echo get_template_directory_uri(); ?>/img/tabs/MDG_small.png 1x, <?php echo get_template_directory_uri(); ?>/img/tabs/MDG_small@2x.png 2x"
								media="(max-width: 1199px)">
						<img src="<?php echo get_template_directory_uri(); ?>/img/tabs/MDG.png"
							 alt="<?php echo $links['mdg']['title']; ?>"
							 title="<?php echo $links['mdg']['title']; ?>">
					</picture>
				</a>
			</li>
			<li><a href="<?php echo $links['dd']['link']; ?>">
					<picture>
						<source srcset="<?php echo get_template_directory_uri(); ?>/img/tabs/DD.png 1x, <?php echo get_template_directory_uri(); ?>/img/tabs/DD@2x.png 2x"
								media="(min-width: 1200px)">
						<source srcset="<?php echo get_template_directory_uri(); ?>/img/tabs/DD_small.png 1x, <?php echo get_template_directory_uri(); ?>/img/tabs/DD_small@2x.png 2x"
								media="(max-width: 1199px)">
						<img src="<?php echo get_template_directory_uri(); ?>/img/tabs/DD.png"
							 alt="<?php echo $links['dd']['title']; ?>"
							 title="<?php echo $links['dd']['title']; ?>">
					</picture>
				</a>
			</li>
			<li><a href="<?php echo $links['bpct']['link']; ?>">
					<picture>
						<source srcset="<?php echo get_template_directory_uri(); ?>/img/tabs/BC.png 1x, <?php echo get_template_directory_uri(); ?>/img/tabs/BC@2x.png 2x"
								media="(min-width: 1200px)">
						<source srcset="<?php echo get_template_directory_uri(); ?>/img/tabs/BC_small.png 1x, <?php echo get_template_directory_uri(); ?>/img/tabs/BC_small@2x.png 2x"
								media="(max-width: 1199px)">
						<img src="<?php echo get_template_directory_uri(); ?>/img/tabs/BC.png"
							 alt="<?php echo $links['bpct']['title']; ?>"
							 title="<?php echo $links['bpct']['title']; ?>">
					</picture>
				</a>
			</li>
			<li><a href="<?php echo $links['swds']['link']; ?>">
					<picture>
						<source srcset="<?php echo get_template_directory_uri(); ?>/img/tabs/SW.png 1x, <?php echo get_template_directory_uri(); ?>/img/tabs/SW@2x.png 2x"
								media="(min-width: 1200px)">
						<source srcset="<?php echo get_template_directory_uri(); ?>/img/tabs/SW_small.png 1x, <?php echo get_template_directory_uri(); ?>/img/tabs/SW_small@2x.png 2x"
								media="(max-width: 1199px)">
						<img src="<?php echo get_template_directory_uri(); ?>/img/tabs/SW.png"
							 alt="<?php echo $links['swds']['title']; ?>"
							 title="S<?php echo $links['swds']['title']; ?>">
					</picture>
				</a>
			</li>
			<li><a href="<?php echo $links['wandc']['link']; ?>">
					<picture>
						<source srcset="<?php echo get_template_directory_uri(); ?>/img/tabs/WS.png 1x, <?php echo get_template_directory_uri(); ?>/img/tabs/WS@2x.png 2x"
								media="(min-width: 1200px)">
						<source srcset="<?php echo get_template_directory_uri(); ?>/img/tabs/WS_small.png 1x, <?php echo get_template_directory_uri(); ?>/img/tabs/WS_small@2x.png 2x"
								media="(max-width: 1199px)">
						<img src="<?php echo get_template_directory_uri(); ?>/img/tabs/WS.png"
							 alt="<?php echo $links['wandc']['title']; ?>"
							 title="<?php echo $links['wandc']['title']; ?>">
					</picture>
				</a>
			</li>
			<li><a href="<?php echo $links['pmatcher']['link']; ?>">
					<picture>
						<source srcset="<?php echo get_template_directory_uri(); ?>/img/tabs/PM.png 1x, <?php echo get_template_directory_uri(); ?>/img/tabs/PM@2x.png 2x"
								media="(min-width: 1200px)">
						<source srcset="<?php echo get_template_directory_uri(); ?>/img/tabs/PM_small.png 1x, <?php echo get_template_directory_uri(); ?>/img/tabs/PM_small@2x.png 2x"
								media="(max-width: 1199px)">
						<img src="<?php echo get_template_directory_uri(); ?>/img/tabs/PM.png"
							 alt="<?php echo $links['pmatcher']['title']; ?>"
							 title="<?php echo $links['pmatcher']['title']; ?>">
					</picture>
				</a>
			</li>
		</ul>
	</div>

	<?php
	$output .= ob_get_clean();

	echo $output;
}

function sw_subscribe() {
	$output = '';
	ob_start();
	?>
	<div class="subscribe">
		<div class="container">
			<div class="subscribe__wrapper">
				<h2>Sign Up for Newsletter</h2>
				<p>
					<?php
					if ( function_exists( 'ot_get_option' ) ) {
						echo ot_get_option( 'sign_up_for_newsletter', ' ' );
					}
					?></p>
				<?php echo do_shortcode( '[contact-form-7 id="2506" title="Sing up footer"]' ); ?>
			</div>
		</div>
	</div>
	<?php

	$output .= ob_get_clean();

	echo $output;
}

function sw_share() {
	$output = '';
	ob_start();
	?>
	<div class="share-list">
		<div class="container">
			<div class="share-list__wrapper">
				<a class="share-list__item" href="#" rel="nofollow" data-count="fb"
				   onclick="window.open(
						   '//www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>',
						   '_blank',
						   'scrollbars=0, resizable=1, menubar=0, left=100, top=100, width=550, height=440, toolbar=0, status=0'
						   );return false" title="<?php the_title(); ?>" target="_blank">
					<svg xmlns="http://www.w3.org/2000/svg" width="13" height="29" viewbox="0 0 13 29"
						 version="1.1">
						<path fill="#FFF" fill-rule="evenodd"
							  d="M3.15 28.63h5.1V14.62h3.827l.756-4.376H8.25v-3.17c0-1.024.662-2.097 1.607-2.097h2.604V.6H9.27v.02C4.274.8 3.25 3.693 3.16 6.73h-.01v3.514H.6v4.377h2.55v14.01z"></path>
					</svg>
					<span>Share</span>
				</a>
				<a class="share-list__item" href="#" rel="nofollow" data-count="twi"
				   onclick="window.open('//twitter.com/intent/tweet?text=<?php the_title(); ?>;url=<?php the_permalink(); ?>', '_blank', 'scrollbars=0, resizable=1, menubar=0, left=100, top=100, width=550, height=440, toolbar=0, status=0');return false"
				   title="Share on Twitter" target="_blank">
					<svg xmlns="http://www.w3.org/2000/svg" width="28" height="23" viewbox="0 0 28 23"
						 version="1.1">
						<path fill="#FFF" fill-rule="evenodd"
							  d="M26.826 1.29c-.79.507-2.605 1.242-3.5 1.242v.002C22.304 1.466 20.866.8 19.27.8c-3.1 0-5.61 2.513-5.61 5.61 0 .43.05.85.14 1.253C9.597 7.553 5 5.446 2.232 1.84c-1.7 2.945-.23 6.22 1.702 7.414-.66.05-1.878-.076-2.45-.635-.04 1.95.9 4.54 4.328 5.48-.66.354-1.828.252-2.336.176.178 1.65 2.49 3.81 5.016 3.81-.9 1.04-4.29 2.93-8.092 2.33 2.582 1.57 5.592 2.48 8.778 2.48 9.053 0 16.084-7.338 15.705-16.39 0-.01 0-.02-.002-.03l.008-.07c0-.03-.002-.056-.003-.083.825-.563 1.93-1.56 2.72-2.873-.457.254-1.832.76-3.11.884.82-.443 2.036-1.894 2.337-3.047"></path>
					</svg>
					<span>Tweet</span>
				</a>
				<a class="share-list__item" rel="nofollow"
				   href="#" data-count="gplus"
				   onclick="window.open('//plus.google.com/share?url=<?php the_permalink(); ?>', '_blank', 'scrollbars=0, resizable=1, menubar=0, left=100, top=100, width=550, height=440, toolbar=0, status=0');return false"
				   title="Share on Google+" target="_blank">
					<svg xmlns="http://www.w3.org/2000/svg" width="31" height="19" viewbox="0 0 31 19"
						 version="1.1">
						<path fill="#FFF" fill-rule="evenodd"
							  d="M.624 8.893C.704 4.18 5.032.05 9.742.208c2.256-.105 4.377.878 6.105 2.258-.737.84-1.5 1.648-2.317 2.403-2.077-1.44-5.032-1.85-7.11-.19-2.972 2.05-3.107 6.91-.25 9.13 2.78 2.52 8.036 1.27 8.805-2.6-1.742-.028-3.488 0-5.23-.058-.003-1.04-.008-2.08-.003-3.12 2.91-.01 5.822-.013 8.737.01.17 2.445-.15 5.05-1.65 7.072-2.28 3.202-6.84 4.137-10.4 2.765C2.85 16.525.32 12.745.62 8.89M24.83 5.8h2.597c0 .87.005 1.743.014 2.613.87.008 1.74.008 2.61.017v2.6c-.87.008-1.73.013-2.61.02-.01.875-.01 1.745-.01 2.614h-2.61c-.006-.87-.006-1.74-.015-2.608-.87-.01-1.742-.018-2.61-.026v-2.6c.868-.01 1.737-.013 2.61-.017.004-.874.013-1.743.022-2.613"></path>
					</svg>
					<span>Share</span></a>
				<a class="share-list__item" href="mailto:?subject=<?php the_title(); ?>&body=<?php the_permalink(); ?>"
				   target="_blank">
					<svg xmlns="http://www.w3.org/2000/svg" width="24" height="18" viewbox="0 0 24 18"
						 version="1.1">
						<g fill="#FFF" fill-rule="nonzero" stroke="none" stroke-width="1">
							<path d="M21.192 17.794c.504 0 .94-.166 1.31-.494l-6.343-6.344-.44.315c-.48.35-.86.63-1.16.82-.3.2-.69.4-1.18.6-.5.21-.95.31-1.38.31h-.03c-.428 0-.886-.1-1.377-.3-.493-.2-.887-.4-1.18-.6-.297-.19-.68-.47-1.157-.82l-.44-.31L1.49 17.3c.37.328.806.494 1.31.494h18.39zM2.062 6.96C1.587 6.645 1.166 6.283.8 5.874v9.653l5.592-5.592c-1.12-.78-2.56-1.77-4.33-2.973zm19.88 0c-1.702 1.153-3.148 2.144-4.34 2.976l5.59 5.59V5.873c-.36.4-.775.763-1.25 1.088z"></path>
							<path d="M21.192.2H2.8c-.642 0-1.136.217-1.48.65-.347.433-.52.975-.52 1.625 0 .524.23 1.093.687 1.705.458.613.946 1.093 1.462 1.443.28.2 1.13.794 2.56 1.78.77.534 1.44.998 2.01 1.4.49.34.91.636 1.26.88l.18.134.34.25c.27.194.495.353.675.474.18.13.397.26.65.41.255.15.495.26.72.34.225.08.433.11.625.11H12c.19 0 .398-.03.623-.11.225-.07.465-.18.72-.33.253-.15.47-.284.65-.404.18-.12.403-.28.673-.474.14-.105.254-.187.344-.25l.19-.135c.27-.19.693-.483 1.262-.88l4.583-3.183c.61-.422 1.12-.935 1.526-1.534.41-.6.618-1.23.618-1.89 0-.55-.196-1.02-.592-1.413-.4-.393-.867-.59-1.41-.59z"></path>
						</g>
					</svg>
					<span>Email</span>
				</a>
			</div>
		</div>
	</div>
	<?php
	$output .= ob_get_clean();
	echo $output;
}

add_filter( 'get_search_form', 'sw_search_form' );

remove_action( 'wp_head', 'wp_shortlink_wp_head' );
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head' );


<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="<?php echo get_template_directory_uri(); ?>/img/favicon-32x32.png" rel="icon" type="image/x-icon">
	<script type="text/javascript">
	//<![CDATA[
    var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
	//]]>
	</script>
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<header class="site-header">
	<div class="container">
		<div class="row">
			<div class="site-header__wrapper">
				<div class="menu">
					<?php
					if ( function_exists( 'ot_get_option' ) ) :
						$header_logo = ot_get_option( 'header_logo' );
					endif;
					?>
						<?php
						wp_nav_menu(
							array(
								'theme_location'  => 'menu-1',
								'menu_id'         => 'primary-menu',
								'container'       => 'div',
								'container_class' => 'menu-nav',
								'items_wrap' => '<ul><li><a href="/"><img src="' . $header_logo .'"
												 srcset="' . $header_logo . ' 1x,' . $header_logo . ' 2x"
												 width="130" height="50" alt=""></a></li>%3$s</ul>'
							)
						);

						?>

					<div class="menu-button is-closed">
						<div class="container">
							<div class="menu-button__wrapper"><span></span><span></span><span></span></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>
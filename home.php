<?php get_header();
$sort_by = "date";
if ( isset( $_GET['order_by'] ) and  strval( $_GET['order_by'] ) == 'title' ):
	$sort_by = strval( $_GET['order_by'] );
endif;
?>
	<main class="site-main site-main--blog clearfix">
		<?php sw_tabs( 'index-tabs--blog' ); ?>
		<div class="blog-title">
			<div class="container">
				<div class="blog-title__wrapper">
					<h1>Blog</h1>
					<div class="blog-title__category">
						<button class="blog-title__category-btn collapsed" type="button" data-toggle="collapse"
								data-target="#categoryDropdown"><span>All Categories</span>
							<svg xmlns="http://www.w3.org/2000/svg" width="7" height="13" data-name="PageArrow"
								 viewbox="0 0 7 13">
								<path class="pageFill"
									  d="M.91 6.5l6-5.87a.36.36 0 0 0 0-.52.38.38 0 0 0-.53 0L.11 6.24a.36.36 0 0 0 0 .52l6.25 6.13a.38.38 0 0 0 .26.11.37.37 0 0 0 .26-.11.36.36 0 0 0 0-.52z"></path>
							</svg>
						</button>
						<div class="collapse blog-title__category-dropdown" id="categoryDropdown">
							<ul>
								<li <?php if ( $sort_by == 'date' or ! $sort_by ): echo 'class="active"'; endif; ?>><a
											href="?order_by=date">Recent</a></li>
								<li <?php if ( $sort_by == 'title' ): echo 'class="active"'; endif; ?>><a
											href="?order_by=title">Alphabetically</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="blog-items latest-blog-posts">
			<div class="container">
				<div class="latest-blog-posts__wrapper">
					<?php echo sw_recent_posts( $posts_count = 3, $posts_offset = 0, $category_name = 'blog', $order_by = $sort_by ); ?>

				</div>
				<a id="infinitescroll-on_click" class="btn btn--turquoise" href="#" data-sort="<?php echo $sort_by; ?>">Show
					More </a>
			</div>
		</div>
		<?php sw_subscribe(); ?>
	</main>
<?php get_footer(); ?>
<?php get_header(); ?>
	<main class="site-main site-main--blog clearfix">
		<?php sw_tabs('index-tabs--blog'); ?>
		<div class="blog-title">
			<div class="container">
				<div class="blog-title__wrapper">
					<h1>Blog</h1>
					<div class="blog-title__category">
						<button class="blog-title__category-btn collapsed" type="button" data-toggle="collapse" data-target="#categoryDropdown"><span>All Categories</span>
							<svg xmlns="http://www.w3.org/2000/svg" width="7" height="13" data-name="PageArrow" viewbox="0 0 7 13">
								<path class="pageFill" d="M.91 6.5l6-5.87a.36.36 0 0 0 0-.52.38.38 0 0 0-.53 0L.11 6.24a.36.36 0 0 0 0 .52l6.25 6.13a.38.38 0 0 0 .26.11.37.37 0 0 0 .26-.11.36.36 0 0 0 0-.52z"></path>
							</svg>
						</button>
						<div class="collapse blog-title__category-dropdown" id="categoryDropdown">
							<ul>
								<li class="active"><a href="#">Recent</a></li>
								<li><a href="#">Alphabetically</a></li>
								<li><a href="#">Most Viewed</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="blog-items latest-blog-posts">
			<div class="container">
				<div class="latest-blog-posts__wrapper">

					<?php echo sw_recent_posts( $posts_count = 6, $posts_offset = 0 ); ?>
				</div>
				<a id="infinitescroll-on_click" class="btn btn--turquoise" href="#">Show More </a>
			</div>
		</div>
		<?php sw_subscribe(); ?>
	</main>
<?php get_footer(); ?>
jQuery(document).ready(function ($) {

    $(window).scroll(function () {

        //fixed-menu
        var scrollPosition = $(window).scrollTop();
        var fixedHeaderMenu = $('.site-header');
        var fixedStartPosition = 1;
        if ((scrollPosition >= fixedStartPosition) && ($(window).width() > '1200')) {
            fixedHeaderMenu.addClass("is-fixed");
        } else {
            fixedHeaderMenu.removeClass("is-fixed");
        }
    });


    window.onload = function () {
        $('.menu-button').click(function () {
            var menu = $('.menu-nav');
            var btn = $('.menu-button');
            // var main = $('.site-main');
            if (btn.hasClass('is-closed')) {
                btn.removeClass('is-closed');
                menu.addClass('is-open');
            }
            else {
                btn.addClass('is-closed');
                menu.removeClass('is-open');
            }
        });

        $('body').waypoint({
            handler: function (direction) {
                if (direction === 'down') {
                    $('#allSites').addClass('active');
                }
                if (direction === 'up') {
                    $('#allSites').removeClass('active');
                }
            },
            offset: '-100%'
        });


        // carousel index--reviews
        var owl = $(".reviews-item__slider");
        owl.owlCarousel({
            slideSpeed: 300,
            paginationSpeed: 400,
            singleItem: true,
            mouseDrag: true,
            touchDrag: true,
            autoPlay: 5000,
            pagination: false,
        });
        $(".reviews-item__slider--right").click(function () {
            owl.trigger('owl.next');
        })
        $(".reviews-item__slider--left").click(function () {
            owl.trigger('owl.prev');
        })

        if (($('#video').length > 0) && ($(window).width() > '1200')) {
            $('.index-first-screen__img').addClass('disabled');
            $('.index-first-screen__video').addClass('active');
            video.loop = true;
            video.play();
        }
    }
});
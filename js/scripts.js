jQuery(document).ready(function ($) {
// events on load
    window.onload = function () {
        openMenu();
        allSites();
        indexSlider();
        scrollToAnchor();
        tourSliders();
    };


// events on scroll
    $(window).scroll(function () {
        fixedHeader();
    });


// events on resize
    window.onresize = function () {
        fixedHeader();
    };

// function declarations
    function openMenu() {
        $('.menu-button').click(function () {
            var menu = $('.menu-nav');
            var btn = $('.menu-button');
            var main = $('.site-main');
            if (btn.hasClass('is-closed')) {
                btn.removeClass('is-closed');
                menu.addClass('is-open');
                main.addClass('menu-is-open');
            }
            else {
                btn.addClass('is-closed');
                menu.removeClass('is-open');
                main.removeClass('menu-is-open');
            }
        });
    }

    function allSites() {
        $('body').waypoint({
            handler: function (direction) {
                if (direction === 'down') {
                    $('#allSites').addClass('active');
                }
                if (direction === 'up') {
                    $('#allSites').removeClass('active');
                }
            },
            offset: '-100%'
        });
    }

    function indexSlider() {
        var owl = $(".index-slider__items");
        owl.owlCarousel({
            slideSpeed: 300,
            paginationSpeed: 400,
            singleItem: true,
            mouseDrag: true,
            touchDrag: true,
            autoPlay: 5000,
            pagination: false,
        });
        $(".index-slider__right").click(function () {
            owl.trigger('owl.next');
        });
        $(".index-slider__left").click(function () {
            owl.trigger('owl.prev');
        })
    }

    function fixedHeader() {
        var scrollPosition = $(window).scrollTop();
        var fixedHeaderMenu = $('.site-header');
        var fixedStartPosition = 1;
        if ((scrollPosition >= fixedStartPosition) && ($(window).width() > '1200')) {
            fixedHeaderMenu.addClass("is-fixed");
        } else {
            fixedHeaderMenu.removeClass("is-fixed");
        }
    }

    function scrollToAnchor() {
        $('.tour-list__links a').bind('click', function (event) {
            var $anchor = $(this);
            $('html, body').stop().animate({
                scrollTop: ($($anchor.attr('href')).offset().top - 80)
            }, 1000);
            event.preventDefault();
        });
    }

    var $infinitescroll_btn = $('#infinitescroll-on_click');

    hide_infinite({action: 'infinite_scroll', offset: get_offset(), sort_by: $infinitescroll_btn.attr("data-sort")});

    function get_offset() {
        return $('.latest-blog-item').length;
    }

    $infinitescroll_btn.on('click', function (event) {
        event.preventDefault();
        var order_by = $(this).attr("data-sort");
        var data = {
            action: 'infinite_scroll',
            offset: get_offset(),
            sort_by: order_by
        };
        jQuery.post(ajaxurl, data, function (response) {
            $('.latest-blog-posts__wrapper').append(response);
            Waypoint.refreshAll();
            hide_infinite({action: 'infinite_scroll', offset: get_offset(), sort_by: order_by});
        });
    });

    function hide_infinite(data) {
        jQuery.post(ajaxurl, data, function (response) {
            if (response.length < 1) {
                $infinitescroll_btn.hide();
            }
        });
    }


    function tourSliders() {
        $(".tour-slider__items").each(function (index, element) {
            var $owl = $(element);
            $owl.owlCarousel({
                slideSpeed: 300,
                paginationSpeed: 400,
                singleItem: true,
                mouseDrag: true,
                touchDrag: true,
                autoPlay: 5000,
                pagination: false

            });

            $owl.next().find(".tour-slider__left").click(function () {
                $owl.trigger('owl.prev');
            });
            $owl.next().find(".tour-slider__right").click(function () {
                $owl.trigger('owl.next');
            });

        });


    }

});
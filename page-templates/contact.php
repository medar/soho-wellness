<?php
/**
 * Template Name: Contact
 *
 * Description: Template for Contact page
 */
get_header();
global $post;
?>

	<main class="site-main site-main--contact">
		<div class="contact-form">
			<div class="container">
				<h1><?php echo esc_html( get_the_title( $post->ID ) ); ?></h1>
				<?php echo do_shortcode('[contact-form-7 id="4" title="Main contact form"]'); ?>
			</div>
		</div>
		<div class="contact-text">
			<?php echo wpautop($post->post_content); ?>

			<a class="btn btn--green" target="_blank" href="<?php echo get_field('button_after_contacts_link'); ?>"><?php echo get_field('button_after_contacts_text'); ?></a>
		</div>
		<div class="contact-map">
			<?php echo do_shortcode('[wpgmza id="1"]'); ?>
			<p><?php echo get_field('text_under_map'); ?></p>
		</div>
		<?php sw_share(); ?>
		<div class="latest-blog-posts">
			<div class="container">
				<div class="latest-blog-posts__wrapper">

					<?php echo sw_recent_posts( $posts_count = 3, $posts_offset = 0 ); ?>
				</div>
			</div>
		</div>
		<?php sw_subscribe(); ?>
	</main>

<?php get_footer(); ?>
<?php
/**
 * Template Name: Services
 *
 * Description: Template for Services page
 */
get_header();
global $post;
?>

	<main class="site-main site-main--services">
		<div class="service-list">
			<div class="container">
				<h1><?php echo esc_html( get_the_title( $post->ID ) ); ?></h1>
				<p><?php echo $post->post_content; ?></p>
				<div class="service-list__wrapper">
					<?php echo sw_services_posts(); ?>

				</div>
			</div>
		</div>
		<?php sw_share(); ?>

		<div class="latest-blog-posts">
			<div class="container">
				<div class="latest-blog-posts__wrapper">

					<?php echo sw_recent_posts( $posts_count = 3, $posts_offset = 0 ); ?>
				</div>
			</div>
		</div>
		<?php sw_subscribe(); ?>

	</main>

<?php get_footer(); ?>
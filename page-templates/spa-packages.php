<?php
/**
 * Template Name: Spa Packages
 *
 * Description: Template for Spa Packages page
 */
get_header();
?>

<main class="site-main site-main--spa-packages">
	<div class="spa-package-list">
		<div class="container">
			<h1><?php echo esc_html( get_the_title( $post->ID ) ); ?></h1>
			<p><?php echo $post->post_content; ?></p>
			<div class="spa-package-list__wrapper">
				<?php
				$args     = array(
					'posts_per_page' => - 1,
					'offset'         => 0,
					'post_type'      => 'sw_packages',
					'order'          => 'ASC',
					'meta_query' => array(
						array(
							'key' => 'hide_on_packages',
							 'compare' => '0' // this should work...
							 ),
						)
					);
				$packages = get_posts( $args );
				?>
				<?php foreach ( $packages as $package ) : ?>

					<?php if ( !get_post_meta( $package->ID, 'hide_on_packages', true ) ) { 

						// var_dump(get_post_meta( $package->ID, 'hide_on_packages' ));

					?>


					<div class="spa-package-item">
							<a class="spa-package-item__wrapper" href="<?php echo get_post_meta( $package->ID, 'button_link', true); ?>" target="_blank">
								<?php echo wp_get_attachment_image(get_post_meta( $package->ID, 'package_image', true), $size=array(400,340)); ?>
							</a><?php get_post_meta( $package->ID, 'hide_on_packages', true); ?>
							<a class="specials-item__btn" href="<?php echo get_post_meta( $package->ID, 'button_link', true); ?>" target="_blank">
								<?php echo get_post_meta( $package->ID, 'button_text', true); ?>
							</a>
						</div>

					
				<?php 
					}

				



				endforeach;
				?>



			</div>
		</div>
	</div>
	<?php sw_share(); ?>
	<div class="latest-blog-posts">
		<div class="container">
			<div class="latest-blog-posts__wrapper">

				<?php echo sw_recent_posts( $posts_count = 3, $posts_offset = 0 ); ?>
			</div>
		</div>
	</div>
	<?php sw_subscribe(); ?>
</main>

<?php get_footer(); ?>
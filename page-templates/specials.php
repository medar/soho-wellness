<?php
/**
 * Template Name: Specials
 *
 * Description: Template for Specials page
 */
get_header();
global $post;
?>

	<main class="site-main site-main--specials">
		<div class="specials-list">
			<div class="container">
				<h1><?php echo esc_html( get_the_title( $post->ID ) ); ?></h1>
				<?php echo $post->post_content; ?>
				<div class="specials-list__wrapper">
					<?php
					$specials = get_field( 'specials' );
					foreach ( $specials as $special ) : ?>
						<div class="specials-item">
							<a class="specials-item__wrapper"
							   href="<?php echo get_post_meta( $special->ID, 'button_link', true ); ?>" target="_blank">
								<?php echo wp_get_attachment_image(get_post_meta( $special->ID, 'package_image', true), $size=array(400,340)); ?>
							</a>
							<a class="specials-item__btn" href="<?php echo get_post_meta( $special->ID, 'button_link', true ); ?>" target="_blank">
								<?php echo get_post_meta( $special->ID, 'button_text', true); ?>
							</a>
						</div>

						<?php
					endforeach;

					?>

				</div>
			</div>
		</div>
		<?php sw_share(); ?>
		<div class="latest-blog-posts">
			<div class="container">
				<div class="latest-blog-posts__wrapper">

					<?php echo sw_recent_posts( $posts_count = 3, $posts_offset = 0 ); ?>
				</div>
			</div>
		</div>
		<?php sw_subscribe(); ?>
	</main>

<?php get_footer(); ?>
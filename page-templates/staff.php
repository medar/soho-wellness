<?php
/**
 * Template Name: Staff
 *
 * Description: Template for Staff page
 */
get_header();
global $post;
?>

	<main class="site-main site-main--about">
		<div class="about-list">
			<p class="container">
			<h1><?php echo esc_html( get_the_title( $post->ID ) ); ?></h1>
			<p><?php echo $post->post_content; ?></p>
			<div class="about-list__wrapper">
				<?php echo sw_staff_posts(); ?>

			</div>
		</div>
		<?php sw_share(); ?>

		<div class="latest-blog-posts">
			<div class="container">
				<div class="latest-blog-posts__wrapper">

					<?php echo sw_recent_posts( $posts_count = 3, $posts_offset = 0 ); ?>
				</div>
			</div>
		</div>
		<?php sw_subscribe(); ?>

	</main>

<?php get_footer(); ?>
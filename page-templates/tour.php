<?php
/**
 * Template Name: Tour
 *
 * Description: Template for Tour page
 */
get_header();
global $post;
?>

<main class="site-main site-main--tour">
	<div class="tour-list">
		<div class="container">
			<h1><?php echo esc_html( get_the_title( $post->ID ) ); ?></h1>
			<?php echo $post->post_content; ?>
			<?php
			$args  = array( 'posts_per_page' => - 1, 'offset' => 0, 'post_type' => 'sw_rooms', 'order' => 'ASC' );
			$rooms = get_posts( $args );
			?>
			<div class="tour-list__links">
				<?php foreach ( $rooms as $room ) : ?>
					<a href="#room-<?php echo $room->ID; ?>"><?php echo esc_html( get_the_title( $room->ID ) ); ?></a>
				<?php endforeach; ?>
			</div>
			<div class="tour-list__wrapper">
				<?php foreach ( $rooms as $room ) : ?>
					<div class="tour-item" id="room-<?php echo $room->ID; ?>">
						<div class="tour-item__img">
							<div class="tour-slider__items">
								<?php
								$slides = 0;
								for ($i = 1; $i <= 3; $i++) {
									if (get_post_meta( $room->ID, "slide{$i}-image", true )) :
										$slides++;
									endif;
								}
								{ 
									for (
										$i = 1;
										$i <= $slides;
										$i ++
									) {
										$image = get_post_meta( $room->ID, "slide{$i}-image", true );
										$text  = get_post_meta( $room->ID, "slide{$i}-text", true );
											?>
											<div class="tour-slider__item">
												<picture>
													<?php echo wp_get_attachment_image( $image, $size = array(
														570,
														370
													) ); ?>
												</picture>
												<div><span><?php echo "{$i} / {$slides}"; ?></span><p><?php echo $text; ?></p></div>
											</div>

											<?php
									}
								}

								?>
							</div>
							<?php if($slides != 1){ ?>
							<div class="tour-slider__controls">
								<div class="container">
								<span class="tour-slider__left">
									<svg xmlns="http://www.w3.org/2000/svg" width="7" height="13" data-name="PageArrow" viewbox="0 0 7 13">
									<path class="pageFill"
											  d="M.91 6.5l6-5.87a.36.36 0 0 0 0-.52.38.38 0 0 0-.53 0L.11 6.24a.36.36 0 0 0 0 .52l6.25 6.13a.38.38 0 0 0 .26.11.37.37 0 0 0 .26-.11.36.36 0 0 0 0-.52z"></path>
									</svg>
								</span>
								<span class="tour-slider__right">
									<svg xmlns="http://www.w3.org/2000/svg" width="7" height="13" data-name="PageArrow" viewbox="0 0 7 13">
									<path class="pageFill"
										  d="M.91 6.5l6-5.87a.36.36 0 0 0 0-.52.38.38 0 0 0-.53 0L.11 6.24a.36.36 0 0 0 0 .52l6.25 6.13a.38.38 0 0 0 .26.11.37.37 0 0 0 .26-.11.36.36 0 0 0 0-.52z"></path>
									</svg>
								</span>
								</div>
							</div>
							<?php } ?>
						</div>
						<div class="tour-item__text">
							<h2><?php echo esc_html( get_the_title( $room->ID ) ); ?></h2>
							<p><?php echo $post->post_content; ?></p>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
	<?php sw_share(); ?>
	<div class="latest-blog-posts">
		<div class="container">
			<div class="latest-blog-posts__wrapper">

				<?php echo sw_recent_posts( $posts_count = 3, $posts_offset = 0 ); ?>
			</div>
		</div>
	</div>
	<?php sw_subscribe(); ?>
</main>


<?php get_footer(); ?>

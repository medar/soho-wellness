<?php get_header();
global $post;
$article_type = 'article';
$categories = get_the_category();
if ( ! empty( $categories ) ) {
	$slug = esc_html( $categories[0]->slug);
	if ($slug  === 'people' or $slug  === 'staff' or $slug  === 'services'  ):
		$article_type = 'text-page';
	endif;
}

?>
<main class="site-main site-main--<?php echo $article_type; ?> clearfix">

	<?php

	while ( have_posts() ) :
		the_post();

		get_template_part( 'template-parts/content', $article_type );

	endwhile;
	?>


	<div class="latest-blog-posts">
		<div class="container">
			<div class="latest-blog-posts__wrapper">
				<?php echo sw_recent_posts( $posts_count = 3, $posts_offset = 0 ); ?>
			</div>
		</div>
	</div>
	<?php sw_subscribe(); ?>
</main>
<?php get_footer(); ?>


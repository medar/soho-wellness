<?php
if ( !has_post_thumbnail() ) {
	$no_image = ' article-img--without-img';
}else{
	$no_image = '';
}
?>
<div class="article-img<?php echo $no_image; ?>">
	<div class="container">
		<div class="row">
			<div class="article-img__wrapper">
				<?php
				if ( has_post_thumbnail() ) {
					echo '<picture>';
					echo get_the_post_thumbnail( $post->ID, 'post-header-full' );
					echo '</picture>';
				} ?>
				<div class="article-img__title">
					<h1><?php echo apply_filters( 'the_title', $post->post_title, $post->ID ); ?></h1>
					<p><?php sw_posted_on($post->post_date); ?></p>

				</div>
			</div>
		</div>
	</div>
</div>
<div class="article-content">
	<div class="article-content__main">
		<?php echo apply_filters( 'the_content', $post->post_content ); ?>
	</div>
	<div class="article-content__sidebar">
		<div class="sidebar-subscribe">
			<?php echo do_shortcode( '[contact-form-7 id="2507" title="Sing up sidebar"]' ); ?>
		</div>
		<div class="sidebar-share">
			<p>Share</p>
			<div class="sidebar-share__wrapper">
				<?php
				if ( function_exists( 'ot_get_option' ) ) {
					$facebook_url = ot_get_option( 'social_facebook_link', '#' );
					$twitter_url  = ot_get_option( 'social_twitter_link', '#' );
					$g_plus_url   = ot_get_option( 'social_google_link', '#' );
					$linkedin_url = ot_get_option( 'social_linkedin_link', '#' );
				}
				?>
				<a href="<?php echo $facebook_url; ?>" target="_blank">
					<img src="<?php echo get_template_directory_uri(); ?>/img/fb.svg" alt="" width="20"
					     height="20">
				</a>
				<a href="<?php echo $twitter_url; ?>" target="_blank">
					<img src="<?php echo get_template_directory_uri(); ?>/img/tw.svg" alt="" width="20"
					     height="18">
				</a>
				<a href="<?php echo $g_plus_url; ?>" target="_blank">
					<img src="<?php echo get_template_directory_uri(); ?>/img/gp.svg" alt="" width="28"
					     height="18">
				</a>
				<a href="<?php echo $linkedin_url; ?>" target="_blank">
					<img src="<?php echo get_template_directory_uri(); ?>/img/li.svg" alt="" width="20"
					     height="18">
				</a>
			</div>

			<?php
			if ( ! function_exists( 'dynamic_sidebar' ) || ! dynamic_sidebar( 'sidebar-1' ) ):
			endif;
			?>
		</div>
	</div>
</div>
<div class="text-page">
	<div class="container">
		<h1><?php echo apply_filters( 'the_title', $post->post_title, $post->ID ); ?></h1>
		
		<?php 
			global $post;
			$category_detail=get_the_category( $post->ID );
		if($category_detail[0]->term_id != 10){ 
		?>
		<div class="text-img__wrapper">
			<?php
				if ( has_post_thumbnail() ) {
					echo '<picture>';
					the_post_thumbnail( $post->ID, array(1170, 500) );
					echo '</picture>';
				} else {
					?>
					<picture class="empty-article-picture">
					</picture>

				<?php } ?>
		</div>
		<?php } ?>
		<div class="text-container">
			<?php echo apply_filters( 'the_content', $post->post_content ); ?>
		</div>
	</div>
</div>
<?php sw_share(); ?>